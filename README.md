# RJOperationKit

[![CI Status](http://img.shields.io/travis/RylanJIN/RJOperationKit.svg?style=flat)](https://travis-ci.org/RylanJIN/RJOperationKit)
[![Version](https://img.shields.io/cocoapods/v/RJOperationKit.svg?style=flat)](http://cocoapods.org/pods/RJOperationKit)
[![License](https://img.shields.io/cocoapods/l/RJOperationKit.svg?style=flat)](http://cocoapods.org/pods/RJOperationKit)
[![Platform](https://img.shields.io/cocoapods/p/RJOperationKit.svg?style=flat)](http://cocoapods.org/pods/RJOperationKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RJOperationKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RJOperationKit"
```

## Author

RylanJIN, xiaojun.jin@outlook.com

## License

RJOperationKit is available under the MIT license. See the LICENSE file for more info.
