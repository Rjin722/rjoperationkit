#
# Be sure to run `pod lib lint RJOperationKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RJOperationKit'
  s.version          = '0.2.4'
  s.summary          = 'RJOperationKit Pod'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                       RJOperationKit's CocoaPods Integration
                       DESC

  s.homepage         = 'https://github.com/RylanJIN'
  # s.screenshots    = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Ryan Jin' => 'xiaojun.jin@outlook.com' }
  s.source           = { :git => 'https://Rjin722@bitbucket.org/Rjin722/rjoperationkit.git', :tag => s.version.to_s }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'RJOperationKit/Frameworks/RJOperationKit.framework/Headers/*.h'

  # s.resource_bundles = {
  #   'RJOperationKit' => ['RJOperationKit/Assets/*.png']
  # }
  s.xcconfig	     	= { 'OTHER_LDFLAGS' => '-ObjC'}
  s.vendored_frameworks = 'RJOperationKit/Frameworks/RJOperationKit.framework'

  s.public_header_files = 'RJOperationKit/Frameworks/RJOperationKit.framework/Headers/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
