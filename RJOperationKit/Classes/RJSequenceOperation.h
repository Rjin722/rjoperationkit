//
//  RJSequenceOperation.h
//  RJOperationKit
//
//  Created by Ryan Jin on 9/7/16.
//  Copyright © 2016 Ryan Jin. All rights reserved.
//

#import "RJGroupOperation.h"

extern NSString * const RJSequenceOtherErrosKey;

@interface RJSequenceOperation : RJGroupOperation

@property (nonatomic, strong) void (^success)(void);
@property (nonatomic, strong) void (^failure)(NSError *error);
@property (nonatomic, strong) dispatch_queue_t resultQueue;

+ (instancetype)groupWithOperations:(NSArray *)operations trivialOperations:(NSArray *)trivialOperations;
- (void)setSuccess:(void (^)(void))success failure:(void (^)(NSError *))failure;

/**
 Operations added as trivial are operation that won't cancel the whole sequence
 in case they fail. A good example is the Location operation that might not have
 permissions or not be available.
 */
- (void)addTrivialOperation:(NSOperation *)operation;

@end
