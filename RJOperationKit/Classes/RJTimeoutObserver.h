//
//  RJTimeoutObserver.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/19/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RJOperation.h"

extern NSString * const RJTimeoutKey;

@interface RJTimeoutObserver : NSObject <RJOperationObserver>

@property (nonatomic, readonly, assign) NSTimeInterval timeout;

- (instancetype)initWithTimeoutInterval:(NSTimeInterval)timeout;

@end
