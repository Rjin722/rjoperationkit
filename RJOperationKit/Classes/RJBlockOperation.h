//
//  RJBlockOperation.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/9/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import "RJOperation.h"

typedef void (^RJOperationCompletionBlock)(NSError *);
typedef void (^RJOperationBlock)(RJOperationCompletionBlock completion);

@interface RJBlockOperation : RJOperation

/**
 The designated initializer.
 
 - parameter block: The block to run when the operation executes. This
 block will be run on an arbitrary queue. The parameter passed to the
 block MUST be invoked by your code, or else the 'BlockOperation'
 will never finish executing. If this parameter is 'nil', the operation
 will immediately finish.
 */
- (instancetype)initWithBlock:(RJOperationBlock)block NS_DESIGNATED_INITIALIZER;
/**
 A convenience initializer to execute a block on the main queue.
 
 - parameter mainQueueBlock: The block to execute on the main queue. Note
 that this block does not have a "continuation" block to execute (unlike
 the designated initializer). The operation will be automatically ended
 after the 'mainQueueBlock' is executed.
 */
- (instancetype)initWithMainQueueBlock:(dispatch_block_t)mainBlock;

+ (instancetype)operationWithBlock:(dispatch_block_t)block;

@end
