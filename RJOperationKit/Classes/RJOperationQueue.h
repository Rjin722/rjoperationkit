//
//  RJOperationQueue.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/9/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RJOperationQueue;

/**
 The delegate of an 'OperationQueue' can respond to 'Operation' lifecycle
 events by implementing these methods.
 
 In general, implementing 'OperationQueueDelegate' is not necessary; you would
 want to use an 'OperationObserver' instead. However, there are a couple of
 situations where using 'OperationQueueDelegate' can lead to simpler code.
 For example, 'GroupOperation' is the delegate of its own internal
 'OperationQueue' and uses it to manage dependencies.
 */

@protocol RJOperationQueueDelegate <NSObject>

- (void)operationQueue:(RJOperationQueue *)queue willAddOperation:(NSOperation *)operation;
- (void)operationQueue:(RJOperationQueue *)queue
    operationDidFinish:(NSOperation *)operation
            withErrors:(NSArray *)errors;
- (void)operationQueueDidFinishAllCurrentTasks:(RJOperationQueue *)queue;

@end

/**
 OperationQueue is an 'NSOperationQueue' subclass that implements a large
 number of "extra features" related to the 'Operation' class:
 
 - Notifying a delegate of all operation completion
 - Extracting generated dependencies from operation conditions
 - Setting up dependencies to enforce mutual exclusivity
 */

@interface RJOperationQueue : NSOperationQueue

@property (nonatomic, weak) id<RJOperationQueueDelegate> delegate;

@end

