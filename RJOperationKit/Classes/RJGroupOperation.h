//
//  RJGroupOperation.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/17/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import "RJOperation.h"

/**
 A subclass of 'Operation' that executes zero or more operations as part of its
 own execution. This class of operation is very useful for abstracting several
 smaller operations into a larger operation. As an example, the 'GetEarthquakesOperation'
 is composed of both a 'DownloadEarthquakesOperation' and a 'ParseEarthquakesOperation'.
 
 Additionally, 'GroupOperation's are useful if you establish a chain of dependencies,
 but part of the chain may "loop". For example, if you have an operation that
 requires the user to be authenticated, you may consider putting the "login"
 operation inside a group operation. That way, the "login" operation may produce
 subsequent operations (still within the outer 'GroupOperation') that will all
 be executed before the rest of the operations in the initial chain of operations.
 */

@interface RJGroupOperation : RJOperation

- (instancetype)initWithOperations:(NSArray *)operations NS_DESIGNATED_INITIALIZER;
+ (instancetype)groupWithOperations:(NSArray *)operations;

- (void)addOperation:(NSOperation *)operation;
- (void)operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors;

- (void)aggregateError:(NSError *)error;

@end
