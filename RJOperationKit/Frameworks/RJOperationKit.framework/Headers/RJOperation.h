//
//  RJOperation.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/8/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const RJOperationKey;

@class RJOperation;

#pragma mark - RJOperationObserver
@protocol RJOperationObserver <NSObject>

@required

// Invoked immediately prior to the Operation's 'execute()' method.
- (void)operationDidStart:(RJOperation *)operation;

// Invoked when 'Operation.produceOperation(_:)' is executed.
- (void)operation:(RJOperation *)operation didProduceOperation:(NSOperation *)newOperation;

/**
 Invoked as an 'Operation' finishes, along with any errors produced during
 execution (or readiness evaluation).
 */
- (void)operationDidFinish:(RJOperation *)operation withErrors:(NSArray *)errors;

@end

#pragma mark - RJOperationCondition
typedef NS_ENUM(NSUInteger, RJOperationConditionResult) {
    RJOperationConditionResultSatisfied,
    RJOperationConditionResultFailed
};

extern NSString * const RJOperationConditionKey;

@protocol RJOperationCondition <NSObject>

/**
 The name of the condition. This is used in userInfo dictionaries of '.ConditionFailed'
 errors as the value of the 'OperationConditionKey' key.
 */
- (NSString *)name;

/**
 Specifies whether multiple instances of the conditionalized operation may
 be executing simultaneously.
 */
- (BOOL)isMutuallyExclusive;

/**
 Some conditions may have the ability to satisfy the condition if another
 operation is executed first. Use this method to return an operation that
 (for example) asks for permission to perform the operation
 
 - parameter operation: The 'Operation' to which the Condition has been added.
 - returns: An 'NSOperation', if a dependency should be automatically added. Otherwise, 'nil'.
 - note: Only a single operation may be returned as a dependency. If you
 find that you need to return multiple operations, then you should be
 expressing that as multiple conditions. Alternatively, you could return
 a single 'GroupOperation' that executes multiple operations internally.
 */
- (NSOperation *)dependencyForOperation:(RJOperation *)operation;

// Evaluate the condition, to see if it has been satisfied or not.
- (void)evaluateForOperation:(RJOperation *)operation
                  completion:(void (^)(RJOperationConditionResult result, NSError *error))completion;

@end

#pragma mark - RJOperation
@interface RJOperation : NSOperation

@property (nonatomic, assign) BOOL userInitiated;

@property (nonatomic, readonly) NSArray *observers;
@property (nonatomic, readonly) NSArray *conditions;
@property (nonatomic, readonly) NSArray *generatedErrors;

- (void)addObserver:(id<RJOperationObserver>)observer;
- (void)addCondition:(id<RJOperationCondition>)condition;

- (void)willEnqueue;

/**
 'execute' is the entry point of execution for all 'RJOperation' subclasses.
 If you subclass 'Operation' and wish to customize its execution, you would
 do so by overriding the 'execute' method.
 
 At some point, your 'RJOperation' subclass must call one of the "finish"
 methods defined below; this is how you indicate that your operation has
 finished its execution, and that operations dependent on yours can re-evaluate
 their readiness state.
 */
- (void)execute;

// Cancel the operation by manually passing along the error(s).
- (void)cancelWithError:(NSError *)error;
- (void)cancelWithErrors:(NSArray *)errors;

/**
 Most operations may finish with a single error, if they have one at all.
 This is a convenience method to simplify calling the actual 'finish: nil'
 method. This is also useful if you wish to finish with an error provided
 by the system frameworks.
 */
- (void)finishWithError:(NSError *)error;
- (void)finish:(NSArray *)errors;
/**
 Subclasses may override 'finished:' if they wish to react to the operation
 finishing with errors. For example, a 'LoadModelOperation' may implement
 this method to potentially inform the user about an error when trying to
 bring up the Core Data stack.
 */
- (void)finished:(NSArray *)errors;

- (void)produceOperation:(NSOperation *)operation;

/**
 A backward compatible way to set the operation's name which won't do anything
 on system versions prior to 8.0
 */
- (void)setSafeName:(NSString *)name;

@end

#pragma mark - RJOperationConditionEvaluator
@interface RJOperationConditionEvaluator : NSObject

+ (void)evaluate:(NSArray *)conditions
       operation:(RJOperation *)operation
      completion:(void (^)(NSArray *errors))completion;

@end

#pragma mark - NSOperation Extension
@interface NSOperation (RJOperation)

- (void)addCompletionBlock:(void (^)(void))block;
- (void)addDependencies:(NSArray *)dependencies;

@end

#pragma mark - NSError Extension
typedef NS_ENUM(NSInteger, RJOperationErrorCode) {
    RJOperationErrorCodeExecution = -11,
    RJOperationErrorCodeCondition = -12
};

extern NSString * const RJOperationErrorDomain;

@interface NSError (RJOperation)

+ (NSError *)executionErrorWithUserInfo:(NSDictionary *)userInfo;
+ (NSError *)conditionErrorWithUserInfo:(NSDictionary *)userInfo;

@end
