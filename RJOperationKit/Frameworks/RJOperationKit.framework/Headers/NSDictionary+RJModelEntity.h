//
//  NSDictionary+RJModelEntity.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/10/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (RJModelEntity)

- (id)objectForKeyNilSafe:(id)aKey;

@end
