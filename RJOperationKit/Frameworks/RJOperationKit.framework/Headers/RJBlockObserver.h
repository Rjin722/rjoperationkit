//
//  RJBlockObserver.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/9/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RJOperation.h"

/**
 The 'BlockObserver' is a way to attach arbitrary blocks to significant events
 in an Operation's lifecycle.
 */

@interface RJBlockObserver : NSObject <RJOperationObserver>

- (instancetype)initWithStartHandler:(void (^)(RJOperation *))startHandler
                      produceHandler:(void (^)(RJOperation *, NSOperation *))produceHandler
                       finishHandler:(void (^)(RJOperation *, NSArray *))finishHandler;

@end
