//
//  RJMutuallyExclusiveCondition.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/10/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RJOperation.h"

@interface RJMutuallyExclusiveCondition : NSObject <RJOperationCondition>

- (instancetype)initWithClass:(Class __unsafe_unretained)class NS_DESIGNATED_INITIALIZER;

+ (instancetype)mutualExclusiveForClass:(Class __unsafe_unretained)class;
+ (instancetype)alertMutuallyExclusive;
+ (instancetype)viewControllerMutuallyExclusive;

@end
