//
//  RJDelayOperation.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/19/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import "RJOperation.h"

/**
 This file shows how to make an operation that efficiently waits.
 
 DelayOperation is an 'Operation' that will simply wait for a given time
 interval, or until a specific 'NSDate'.
 
 It is important to note that this operation does NOT use the 'sleep()'
 function, since that is inefficient and blocks the thread on which it is called.
 Instead, this operation uses 'dispatch_after' to know when the appropriate amount
 of time has passed.
 
 If the interval is negative, or the 'NSDate' is in the past, then this operation
 immediately finishes.
 */

@interface RJDelayOperation : RJOperation

- (instancetype)initWithDelayInterval:(NSTimeInterval)delay;
- (instancetype)initWithDelayUntilDate:(NSDate *)date;

@end
