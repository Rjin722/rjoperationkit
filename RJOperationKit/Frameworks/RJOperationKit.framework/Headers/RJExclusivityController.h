//
//  RJExclusivityController.h
//  OperationDemo
//
//  Created by Ryan Jin on 12/15/15.
//  Copyright © 2015 ArcSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RJOperation.h"

/**
 ExclusivityController is a singleton to keep track of all the in-flight
 'Operation' instances that have declared themselves as requiring mutual exclusivity.
 We use a singleton because mutual exclusivity must be enforced across the entire
 app, regardless of the 'OperationQueue' on which an 'Operation' was executed.
 */

@interface RJExclusivityController : NSObject

+ (instancetype)sharedExclusivityController;

- (void)addOperation:(RJOperation *)operation categories:(NSArray *)categories;
- (void)removeOperation:(RJOperation *)operation categories:(NSArray *)categories;

@end
