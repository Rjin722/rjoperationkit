//
//  RJAppDelegate.h
//  RJOperationKit
//
//  Created by RylanJIN on 03/18/2017.
//  Copyright (c) 2017 RylanJIN. All rights reserved.
//

@import UIKit;

@interface RJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
